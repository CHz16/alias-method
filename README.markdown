Implementation of the [alias method of sampling from a discrete probability distribution](https://en.wikipedia.org/wiki/Alias_method), as described here: http://www.keithschwarz.com/darts-dice-coins/. There are a couple of other, less clever samplers here too just for comparison.

This is just something I did for fun because the algorithm looked cute, and so I make no guarantees about its mathematical correctness or performance. That is: don't actually use this in anything; instead find code written by someone who knows what they're doing.

`test.sh` makes a random distribution and then generates a whole bunch of samples to see if the samplers produce values vaguely close to what they should. Again though, I make no guarantees about the mathematical correctness of this (especially w.r.t. numerical stability and edge cases).

`benchmark.sh` does some runtime tests and prints construction time, sampling time, and total time for a combination of distribution sizes and sample counts. Here was a run I did:

```
Distribution size: 2 | Samples: 1
LinearWalkSampler    0.0                    2.62260437011719e-06  2.62260437011719e-06
BinarySearchSampler  9.5367431640625e-07    9.77516174316406e-07  1.95503234863281e-06
AliasMethodSampler   9.98973846435547e-06   1.00135803222656e-06  1.11818313598633e-05

Distribution size: 2 | Samples: 1000
LinearWalkSampler    0.0                    0.00114119052886963   0.00114119052886963
BinarySearchSampler  1.76429748535156e-06   0.000754833221435547  0.000756597518920898
AliasMethodSampler   1.10149383544922e-05   0.000410580635070801  0.000421404838562012

Distribution size: 2 | Samples: 100000
LinearWalkSampler    3.814697265625e-07     0.121377801895142     0.121378207206726
BinarySearchSampler  1.62124633789062e-06   0.0467144250869751    0.0467159986495972
AliasMethodSampler   1.77860260009766e-05   0.0407997846603394    0.0408209800720215

Distribution size: 10 | Samples: 1
LinearWalkSampler    0.0                    4.43458557128906e-06  4.43458557128906e-06
BinarySearchSampler  2.98023223876953e-06   1.16825103759766e-06  3.95774841308594e-06
AliasMethodSampler   5.74350357055664e-05   1.97887420654297e-06  5.96284866333008e-05

Distribution size: 10 | Samples: 1000
LinearWalkSampler    4.05311584472656e-07   0.00257079601287842   0.0025712251663208
BinarySearchSampler  2.02655792236328e-06   0.000643014907836914  0.000645017623901367
AliasMethodSampler   4.80175018310547e-05   0.000687003135681152  0.000734400749206543

Distribution size: 10 | Samples: 100000
LinearWalkSampler    7.86781311035156e-07   0.263813757896423     0.263814377784729
BinarySearchSampler  2.98023223876953e-06   0.0667499780654907    0.0667531728744507
AliasMethodSampler   3.76224517822266e-05   0.0424381732940674    0.0424721717834473

Distribution size: 1000 | Samples: 1
LinearWalkSampler    9.77516174316406e-07   0.000218415260314941  0.000219225883483887
BinarySearchSampler  2.56061553955078e-05   2.81333923339844e-06  3.1590461730957e-05
AliasMethodSampler   0.00229802131652832    2.57492065429688e-06  0.0023007869720459

Distribution size: 1000 | Samples: 1000
LinearWalkSampler    3.814697265625e-07    0.176790642738342      0.176791429519653
BinarySearchSampler  1.8763542175293e-05   0.00137519836425781    0.00139396190643311
AliasMethodSampler   0.0022712230682373    0.000455164909362793   0.00275619029998779

Distribution size: 1000 | Samples: 100000
LinearWalkSampler    9.5367431640625e-07   17.8324780225754       17.8324788093567
BinarySearchSampler  2.00033187866211e-05  0.14280378818512       0.142823576927185
AliasMethodSampler   0.00211300849914551   0.0444251775741577     0.046535587310791
```
