protocol DiscreteDistributionSampler {
    init(distribution: [Double], randomNumberGenerator: RandomNumberGenerator)

    mutating func sample() -> Int
}


// Do no preprocessing; every time we generate a sample, just walk along the distributions from start to finish to find what bucket the number's in.

struct LinearWalkSampler : DiscreteDistributionSampler {

    let distribution: [Double]
    var randomNumberGenerator: RandomNumberGenerator

    mutating func sample() -> Int {
        var d = randomNumberGenerator.double()
        for (i, probability) in distribution.enumerated() {
            d -= probability
            if d <= 0 {
                return i
            }
        }
        return distribution.count - 1
    }
}


// Calculate the cumulative distribution and then binary search on that to generate samples.

struct BinarySearchSampler : DiscreteDistributionSampler {

    let cumulativeDistribution: [Double]
    var randomNumberGenerator: RandomNumberGenerator

    init(distribution: [Double], randomNumberGenerator: RandomNumberGenerator) {
        var cumulativeDistribution: [Double] = [distribution[0]]
        for i in 1..<distribution.count {
            cumulativeDistribution.append(cumulativeDistribution[i - 1] + distribution[i])
        }
        self.cumulativeDistribution = cumulativeDistribution
        self.randomNumberGenerator = randomNumberGenerator
    }

    mutating func sample() -> Int {
        let d = randomNumberGenerator.double()

        var l = 0, r = cumulativeDistribution.count - 1
        while l <= r {
            let m = (l + r) / 2
            if cumulativeDistribution[m] < d {
                l = m + 1
            } else if cumulativeDistribution[m] > d {
                r = m - 1
            } else {
                return m
            }
        }
        return l
    }

}


// Vose's alias method

struct AliasMethodSampler : DiscreteDistributionSampler {

    let aliasProbabilities: [Double]
    let aliases: [Int]
    var randomNumberGenerator: RandomNumberGenerator

    init(distribution: [Double], randomNumberGenerator: RandomNumberGenerator) {
        self.randomNumberGenerator = randomNumberGenerator

        var small: [Int] = [], large: [Int] = []
        var adjustedProbabilities = distribution.map { $0 * Double(distribution.count) }
        for (i, p) in adjustedProbabilities.enumerated() {
            if p < 1 {
                small.append(i)
            } else {
                large.append(i)
            }
        }

        var aliasProbabilities = Array(repeating: 0.0, count: distribution.count)
        var aliases = Array(repeating: 0, count: distribution.count)
        while !small.isEmpty && !large.isEmpty {
            let l = small.removeLast(), g = large.removeLast()
            aliasProbabilities[l] = adjustedProbabilities[l]
            aliases[l] = g

            let newProbability = adjustedProbabilities[g] + adjustedProbabilities[l] - 1
            adjustedProbabilities[g] = newProbability
            if newProbability < 1 {
                small.append(g)
            } else {
                large.append(g)
            }
        }

        for i in small {
            aliasProbabilities[i] = 1.0
        }
        for i in large {
            aliasProbabilities[i] = 1.0
        }
        self.aliasProbabilities = aliasProbabilities
        self.aliases = aliases
    }

    mutating func sample() -> Int {
        let d = randomNumberGenerator.double() * Double(aliases.count)
        let bucket = Int(d), prob = d.truncatingRemainder(dividingBy: 1)
        if prob <= aliasProbabilities[bucket] {
            return bucket
        } else {
            return aliases[bucket]
        }
    }

}
