func makeRandomDistribution(length: Int) -> [Double] {
    var randomNumberGenerator = Rand48RandomNumberGenerator()
    var poles: [Double] = [0, 1]
    for _ in 0..<(length - 1) {
        poles.append(randomNumberGenerator.double())
    }
    poles = poles.sorted()

    var distribution: [Double] = []
    for i in 1..<poles.count {
        distribution.append(poles[i] - poles[i - 1])
    }
    return distribution
}


let timesToRun = 9, extremesToDiscard = 2
let distributionSizes = [2, 10, 1000]
let sampleCounts = [1, 1000, 100000]
let samplers: [DiscreteDistributionSampler.Type] = [LinearWalkSampler.self, BinarySearchSampler.self, AliasMethodSampler.self]


for distributionSize in distributionSizes {
    for sampleCount in sampleCounts {
        print("Distribution size: \(distributionSize) | Samples: \(sampleCount)")
        for samplerType in samplers {
            var setupTimes: [Double] = [], samplingTimes: [Double] = [], runtimes: [Double] = []
            for _ in 0..<timesToRun {
                let distribution = makeRandomDistribution(length: distributionSize)
                let randomNumberGenerator = Rand48RandomNumberGenerator()

                let startTime = CFAbsoluteTimeGetCurrent()
                var sampler = samplerType.init(distribution: distribution, randomNumberGenerator: randomNumberGenerator)
                let setupTime = CFAbsoluteTimeGetCurrent()
                for _ in 0..<sampleCount {
                    _ = sampler.sample()
                }
                let endTime = CFAbsoluteTimeGetCurrent()

                setupTimes.append(Double(setupTime - startTime))
                samplingTimes.append(Double(endTime - setupTime))
                runtimes.append(Double(endTime - startTime))
            }

            let middleSetupTimes = setupTimes.sorted().dropFirst(extremesToDiscard).dropLast(extremesToDiscard)
            let averageSetupTime = middleSetupTimes.reduce(0.0, +) / Double(middleSetupTimes.count)
            let middleSamplingTimes = samplingTimes.sorted().dropFirst(extremesToDiscard).dropLast(extremesToDiscard)
            let averageSamplingTime = middleSamplingTimes.reduce(0.0, +) / Double(middleSamplingTimes.count)
            let middleRuntimes = runtimes.sorted().dropFirst(extremesToDiscard).dropLast(extremesToDiscard)
            let averageRuntime = middleRuntimes.reduce(0.0, +) / Double(middleRuntimes.count)
            print("\(samplerType)\t\(averageSetupTime)\t\(averageSamplingTime)\t\(averageRuntime)")
        }
        print("")
    }
}
