import Foundation


// This RNG implementation is just copied from another project

protocol RandomNumberGenerator {
    init(seed: Int)
    init()

    mutating func double() -> Double
}

struct Rand48RandomNumberGenerator: RandomNumberGenerator {

    var seedBuffer: [UInt16] = []

    init(seed: Int) {
        var seed = seed
        for _ in 0..<3 {
            seedBuffer.insert(UInt16(seed & Int(UInt16.max)), at: 0)
            seed = seed >> 16
        }
    }

    init() {
        // arc4random_uniform returns a 32 bit number, but we need 48 bits for erand48's seed, so we can't just generate the seed with one call. Technically we could do this with two, but for cleanliness we'll just use three.
        let randmax = UInt32(UInt16.max)
        for _ in 0..<3 {
            seedBuffer.append(UInt16(arc4random_uniform(randmax)))
        }
    }

    mutating func double() -> Double {
        return erand48(&seedBuffer)
    }

}
