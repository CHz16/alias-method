import Darwin.ncurses


func makeRandomDistribution(length: Int) -> [Double] {
    var randomNumberGenerator = Rand48RandomNumberGenerator()
    var poles: [Double] = [0, 1]
    for _ in 0..<(length - 1) {
        poles.append(randomNumberGenerator.double())
    }
    poles = poles.sorted()

    var distribution: [Double] = []
    for i in 1..<poles.count {
        distribution.append(poles[i] - poles[i - 1])
    }
    return distribution
}


let distribution = makeRandomDistribution(length: 8)

let samplerNames = ["Walk", "Binary", "Alias"]
var samplers: [DiscreteDistributionSampler] = [LinearWalkSampler(distribution: distribution, randomNumberGenerator: Rand48RandomNumberGenerator()), BinarySearchSampler(distribution: distribution, randomNumberGenerator: Rand48RandomNumberGenerator()), AliasMethodSampler(distribution: distribution, randomNumberGenerator: Rand48RandomNumberGenerator())]
var samples: [[Int]] = []
for _ in 0..<samplers.count {
    samples.append(Array(repeating: 0, count: distribution.count))
}
var totalSamples = 0


func drawChart() {
    clear()

    move(0, 0)
    addstr("Samples: \(totalSamples)")

    move(1, 4)
    addstr("Prob")
    for i in 0..<samplers.count {
        move(1, 13 + 9 * Int32(i))
        addstr(samplerNames[i])
    }

    for i in 0..<distribution.count {
        move(2 + Int32(i), 0)
        addstr(String(i))
        move(2 + Int32(i), 4)
        addstr(String(format: "%.3f", distribution[i] * 100))

        for j in 0..<samplers.count {
            move(2 + Int32(i), 13 + 9 * Int32(j))
            addstr(String(format: "%.3f", Double(samples[j][i]) * 100 / Double(totalSamples)))
        }
    }

    move(3 + Int32(distribution.count), 0)
    addstr("Ctrl-C to quit")

    refresh()
}


signal(SIGINT) { s in
    endwin()
    exit(0)
}

initscr()
noecho()
curs_set(0)

while true {
    for i in 0..<samplers.count {
        samples[i][samplers[i].sample()] += 1
    }
    totalSamples += 1

    if totalSamples % 10000 == 0 {
        drawChart()
    }
}
